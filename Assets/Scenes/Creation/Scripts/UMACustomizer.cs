﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UMA.CharacterSystem.Examples
{
    public class UMACustomizer : MonoBehaviour
    {
        //SharedColorTableItem makes it possible to have more color tables than just skin/hair/cloth
        //So a slot has a shared color name and this iterates over the shared color tables to see if a table with that name exists
        //if it does it uses it, if not it uses the GenericColorList

        private DynamicCharacterAvatar Avatar;

        public TMP_Dropdown changeRaceDropdown;

        List<string> raceDropdownOptions;

        public GameObject raceDropdownPrefab;

        public DNAPanel faceEditor;
        public DNAPanel bodyEditor;

        public UMAMouseOrbit Orbitor;

        string thisRace;

        public void Start()
        {
            SetAvatar(gameObject);
            SetUpRacesDropdown();
            Avatar.CharacterCreated.AddListener(Init);
        }

        public void Init(UMAData umaData)
        {
            Avatar.CharacterCreated.RemoveListener(Init);
            thisRace = Avatar.activeRace.name;
        }

        public void SetAvatar(GameObject newAvatarObject)
        {
            if (Avatar == null || newAvatarObject != Avatar.gameObject)
            {
                Avatar = newAvatarObject.GetComponent<DynamicCharacterAvatar>();
                if (Avatar != null)
                {
                    if (Orbitor != null)
                    {
                        Orbitor.SwitchTarget(Avatar.transform);
                    }
                    thisRace = Avatar.activeRace.name;
                }
                else
                {
                    //Its the Overview target object
                    if (Orbitor != null)
                    {
                        Orbitor.SwitchTarget(newAvatarObject.transform);
                        Orbitor.distance = 1.5f;
                    }
                    Avatar = null;
                }
            }
            else
            {
                if (newAvatarObject == Avatar.gameObject)
                    return;
                if (Orbitor != null)
                {
                    Orbitor.SwitchTarget(newAvatarObject.transform);
                    Orbitor.distance = 1.5f;
                }
                Avatar = null;
            }
        }
        public void SetUpRacesDropdown(string selected = "")
        {
            string newSelected = selected;
            if (newSelected == "")
            {
                if (Avatar != null)
                {
                    newSelected = Avatar.activeRace.name;
                }
            }
            changeRaceDropdown.options.Clear();
            changeRaceDropdown.onValueChanged.RemoveListener(ChangeRace);
            var raceDropdownOptionsArray = Avatar.context.GetAllRacesBase();
            raceDropdownOptions = new List<string>();

            foreach (RaceData r in raceDropdownOptionsArray)
            {
                if (r.raceName != "PlaceholderRace" && r.raceName != "RaceDataPlaceholder")
                    raceDropdownOptions.Add(r.raceName);
            }
            for (int i = 0; i < raceDropdownOptions.Count; i++)
            {
                var thisddOption = new TMP_Dropdown.OptionData();
                thisddOption.text = raceDropdownOptions[i];
                changeRaceDropdown.options.Add(thisddOption);
            }
            for (int i = 0; i < raceDropdownOptions.Count; i++)
            {
                if (raceDropdownOptions[i] == newSelected)
                {
                    changeRaceDropdown.value = i;
                }
            }
            changeRaceDropdown.onValueChanged.AddListener(ChangeRace);
            bodyEditor.transform.parent.gameObject.SetActive(false);
            faceEditor.transform.parent.gameObject.SetActive(false);
            changeRaceDropdown.transform.parent.gameObject.SetActive(true);
        }

        public void ChangeRace(string racename)
        {
            if (Avatar == null)
                return;
            int raceInt = -1;
            for (int i = 0; i < changeRaceDropdown.options.Count; i++)
            {
                if (changeRaceDropdown.options[i].text == racename)
                {
                    raceInt = i;
                    break;
                }
            }
            if (raceInt != -1)
                ChangeRace(raceInt);
            else
            {
                //this must be a newly Downloaded Race so just let CharacterAvatar deal with it...
                DynamicCharacterAvatar.ChangeRaceOptions thisLoadOptions = DynamicCharacterAvatar.ChangeRaceOptions.none;
                Avatar.ChangeRace(racename, thisLoadOptions);
            }
        }
        public void ChangeRace(int raceId)
        {
            var RaceToSet = raceDropdownOptions[raceId];
            if (RaceToSet != Avatar.activeRace.name)
            {
                thisRace = RaceToSet;
                //Force CharacterSystem to find the new race - unless its None Set
                if (RaceToSet != "None Set")
                    UMAContextBase.Instance.GetRace(RaceToSet);
                DynamicCharacterAvatar.ChangeRaceOptions thisLoadOptions = DynamicCharacterAvatar.ChangeRaceOptions.none;
                Avatar.ChangeRace(RaceToSet, thisLoadOptions);
            }
        }

        public void CloseAllPanels()
        {
            faceEditor.transform.parent.gameObject.SetActive(false);
            bodyEditor.transform.parent.gameObject.SetActive(false);
            changeRaceDropdown.transform.parent.gameObject.SetActive(false);
        }

        public void ShowHideFaceDNA()
        {
            faceEditor.Initialize(Avatar);
            if (Orbitor != null)
            {
                TargetFace();
            }
            faceEditor.transform.parent.gameObject.SetActive(true);
            bodyEditor.transform.parent.gameObject.SetActive(false);
            changeRaceDropdown.transform.parent.gameObject.SetActive(false);
        }

        public void ShowHideBodyDNA()
        {
            bodyEditor.Initialize(Avatar);
            if (Orbitor != null)
            {
                TargetBody();
            }
            bodyEditor.transform.parent.gameObject.SetActive(true);
            faceEditor.transform.parent.gameObject.SetActive(false);
            changeRaceDropdown.transform.parent.gameObject.SetActive(false);
        }

        /// <summary>
        /// Point the mouse orbitor at the body center
        /// </summary>
        public void TargetBody()
        {
            if (Orbitor != null)
            {
                Orbitor.distance = 1.4f;
                Orbitor.TargetBone = UMAMouseOrbit.targetOpts.Chest;
            }
        }

        /// <summary>
        /// Point the mouse orbitor at the neck, so you can see the face.
        /// </summary>
        public void TargetFace()
        {
            if (Orbitor != null)
            {
                Orbitor.distance = 0.5f;
                Orbitor.TargetBone = UMAMouseOrbit.targetOpts.Head;
            }
        }
    }
}
