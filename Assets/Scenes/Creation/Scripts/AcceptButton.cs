﻿using TMPro;
using UnityEngine;

public class AcceptButton : MonoBehaviour
{
    public TextMeshProUGUI surname;
    public TextMeshProUGUI familyName;
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    public void StartGame()
    {
        Debug.Log("Change the scene");
    }
}
